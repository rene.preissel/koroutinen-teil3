package p07_koroutinen

import de.e2.koroutine3.createCollageAsyncAwait
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.cancel
import kotlinx.coroutines.cancelAndJoin
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.reactive.awaitFirst
import kotlinx.coroutines.reactor.flux
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.suspendCancellableCoroutine
import kotlinx.coroutines.time.delay
import kotlinx.coroutines.yield
import org.glassfish.jersey.client.ClientConfig
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import reactor.core.publisher.Flux
import java.awt.image.BufferedImage
import java.io.File
import java.io.InputStream
import java.time.Duration
import javax.imageio.ImageIO
import javax.ws.rs.client.ClientBuilder
import javax.ws.rs.client.InvocationCallback
import javax.ws.rs.core.MediaType
import kotlin.concurrent.thread
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class StructuredConcurrencyTests {

    @Test
    fun `01 Go statement considred harmful`() = runBlocking {
        fun saveImage(image: BufferedImage, file: File) {
            //startet einen neuen Thread
            thread { // entspricht „go“
                ImageIO.write(image, "png", file)
            }
        }

        fun sendEMailWithImage(file: File) = Unit

        suspend fun createCollage() {
            val image: BufferedImage = createCollageAsyncAwait("dog",10)
            val file = File("dogs.png")
            saveImage(image, file)
            sendEMailWithImage(file)
        }
    }


    @Test
    fun `02 CoroutineScopes`() = runBlocking {
        suspend fun launchTwoChildren(): String {
            // launch geht hier nicht
            val result = coroutineScope {
                // this ist CoroutineScope
                launch { /* B1 */ }
                launch { /* B2 */ }
                "result"
            }
            return result
        }

        val scope = CoroutineScope(Dispatchers.Default)
        scope.launch {
            // A - this ist CoroutineScope
            launch { /* A1 */ }
        }
        scope.launch {
            // B - this ist CoroutineScope
            launchTwoChildren()
        }
    }

    @Test
    fun `03 Finally for cleanup`() = runBlocking<Unit> {

        coroutineScope {
            launch {
                try {
                    delay(Duration.ofSeconds(2))
                    println("Never")
                } finally {
                    println("Cleanup")
                }
            }

            delay(Duration.ofSeconds(1))
            throw IllegalStateException()
        }

    }


    @Test
    fun `04 Async Integration`() = runBlocking<Unit> {

        val jerseyClient = ClientBuilder.newClient(ClientConfig())

        suspend fun requestImageData(imageUrl: String) = suspendCoroutine<BufferedImage> { cont ->
            jerseyClient.target(imageUrl)
                .request(MediaType.APPLICATION_OCTET_STREAM)
                .async()
                .get(object : InvocationCallback<InputStream> {
                    override fun completed(response: InputStream) {
                        val image = ImageIO.read(response)
                        cont.resume(image)
                    }

                    override fun failed(throwable: Throwable) {
                        cont.resumeWithException(throwable)
                    }
                })
        }
    }

    @Test
    fun `05 Cancellable Async Integration`() = runBlocking<Unit> {

        val jerseyClient = ClientBuilder.newClient(ClientConfig())

        suspend fun requestImageData(imageUrl: String) = suspendCancellableCoroutine<BufferedImage> { cont ->
            val jerseyFuture = jerseyClient.target(imageUrl)
                .request(MediaType.APPLICATION_OCTET_STREAM)
                .async()
                .get(object : InvocationCallback<InputStream> {
                    override fun completed(response: InputStream) {
                        val image = ImageIO.read(response)
                        cont.resume(image)
                    }

                    override fun failed(throwable: Throwable) {
                        cont.resumeWithException(throwable)
                    }
                })

            cont.invokeOnCancellation {
                jerseyFuture.cancel(true)
            }
        }
    }

    @Test
    fun `06 Integration Flux`() = runBlocking<Unit> {

        val scope = CoroutineScope(Dispatchers.Default)
        val intFlux: Flux<Int> = scope.flux {
            repeat(5) { idx ->
                send(idx)
                delay(Duration.ofSeconds(1))
            }
            close()
        }

        intFlux.map { i -> i * i }.subscribe { i ->
            println(i)
        }
        scope.coroutineContext[Job]?.children?.forEach { it.join() }
        scope.cancel()

        intFlux.awaitFirst()
    }

}


