# Beispiele zum Artikel "Koroutinen in Kotlin: Structured Concurrency" im JavaMagazin

Unter *src/test/kotlin* findet sich eine Testklasse, aus denen die freistehenden Beispiele entnommen worden sind.

Unter *main/src/kotlin* finden sich die Datei *CollageFunktionen* in der
alle Koroutinen für die Collage enthalten sind. 